import os
import json
import pprint
import hashlib
from datetime import datetime
import copy
import zipfile
import sys

def getPathJson(path):
    structure = {'name':os.path.basename(path)}
    if os.path.isdir(path):
        structure['type']= 'directory'
        structure['children']=[getPathJson(os.path.join(path,x)) for x in os.listdir(path)]
    else:
        structure['type']='file'
        hasher = hashlib.md5()
        with open(path, 'rb') as f:
            for chunck in iter(lambda: f.read(), b''):
                hasher.update(chunck)
            structure[ 'checksum'] = hasher.hexdigest()
    return structure

def archive(path):
    time  = datetime.now()
    fileName = path.split('/')[-1] + '_' + time.strftime('%y.%m.%d.%H.%M.%S')
    metaPath = path + '/.meta'
    if not os.path.isdir(metaPath):
        os.makedirs(metaPath)
    metaInfos = os.listdir(metaPath)
    if len(metaInfos) == 0:
        with open(metaPath + '/.' + fileName, 'w') as f:
            pass
        metaContent = getPathJson(path)
        with open(metaPath + '/.' + fileName, 'w') as f:
            f.write(json.dumps(metaContent))
        createArchive(metaContent,fileName + '.zip',path)
    else:
        metaFilesDates= [name.split('_')[-1] for name in metaInfos]
        metaFilesDates.sort()
        latestMeta = metaPath + '/.' + path.split('/')[-1]+'_' + metaFilesDates[-1]
        with open(metaPath + '/.' + fileName, 'w') as f:
            pass
        with open(latestMeta) as f:
            latestState = json.loads(f.read())
        currentState = getPathJson(path)
        revisedState = updateState(latestState,latestMeta,currentState)
        with open(metaPath + '/.' + fileName, 'w') as f:
            f.write(json.dumps(revisedState))
        createArchive(revisedState, fileName + '.zip', path)
        

def createArchive(metaContent, zipFile,path):
    for item in metaContent['children']:
        if item['type']== 'file' and not 'ref' in item:
            with zipfile.ZipFile(zipFile, 'a') as archive:
                archive.write(path + '/' + item['name'])
        if item['type']=='directory':
            createArchive(item,zipFile, path + '/' + item['name'])


def updateState(latestState,latestMeta,currentState):
    result = copy.deepcopy(currentState)
    for index, currentFile in enumerate(result['children']):
        latestFile = [file for file in latestState['children'] if file['name']== currentFile['name']]
        if not len(latestFile) == 0:
            if currentFile['type'] == 'directory':
                if not currentFile['name']=='.meta':
                    result['children'][index]= updateState(latestFile[0],latestMeta,currentFile)
            else:
                if latestFile[0]['checksum']==currentFile['checksum']:
                    if 'ref' in latestFile[0]:
                        result['children'][index]['ref']= latestFile[0]['ref']
                    else:
                        result['children'][index]['ref']=latestMeta
            
    return result
    
    

def main():
    testPath='./testfolder'
    #pprint.pprint(getPathJson(testPath))
    #if len(sys.argv)<2:
       # print('not enough params. i need a folder name')
   # else:
    #    archive(sys.argv[1])
        
    archive(testPath)

if __name__ == '__main__':
    main()